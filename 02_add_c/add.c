// Descripción: Suma dos número en lenguaje C.
//      Devuelve el resultado como valor de retorno.
//
// Objetivo:
//      * Mostrar un ejemplo sencillo de programación en lenguaje C.
//      * Comparar el código ensamblador generado por el compilador de C
//        con el código escrito en ensamblador directamente.

long main()
{
    long a, d;      // 'long' son 64 bits con GCC en x86-64

    a = 10;
    d = 14;
    d = a + d;

    return d;       // valor devuelto al sistema operativo
}

/*
    EJERCICIO:

    1. Compilar el programa con el comando:

        $ gcc -o add add.c

       gcc hará automáticamente el proceso en tre etapas:
       1. Compilar: convertir el código C en código ensamblador
       2. Ensamblar: convertir el código ensamblador en código máquina
          (invocando a 'as').
       3. Enlazar: combinar el código y generar el archivo ejecutable
          (invocando a 'ld').

    2. Ejecuta el programa y comprueba el valor devuelto al sistema operativo:

        $ ./add
        $ echo $?
        24

    3. Ejecuta el compilador con la opción '-S' para que sólo compile y genere
       código ensamblador. Compara este código con el código escrito para el
       ejemplo 1. Intenta reconocer los lugares donde el compilador ha guardado
       las variables 'a' y 'b' y dónde ha calculado la operación de suma.

        $ gcc -S -o add.s add.c
        ...
 */
