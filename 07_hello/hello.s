# Descripción:  escribe un mensaje en el terminal usando la función
#               'printf' de la biblioteca de C
#
# Objetivos:
#   * Ensamblar programas usando el compilador GCC.
#   * Llamadas a funciones de la biblioteca estándar de C desde
#     código ensamblador.
#   * Uso del convenio de paso de parámetros a funciones.
#   * Comprender la diferencia entre el enlazado estático y dinámico de
#     programas.
#
# Notas:
#   * La lista de parámetros en llamadas a funciones en x86-64 se pasan a la
#     función en los siguientes registros en el orden mostrado:
#       rdi, rsi, rdx, rcx, r8, r9
#   * Cuando los parámetros son números enteros %rax debe valer cero.
#   * El valor retornado por la función se devuelve en el registro %rax si es
#     de 64 bits y en la combinación de registros rdx:rax si es de hasta 128
#     bits.
#   * El puntero de pila %rsp debe estar "alineado" a direcciones múltiplo de 16
#     antes de llamar a cualquier función de la biblioteca de C.
#   * rbx, rbp y r12-r15 son 'callee-saved', so valor no debe cambiar al
#     regresar de la función llamada por lo que printf no cambiará su valor.
#     El resto de registros pueden ser modificados por la función.

.section .data
message:                        # printf necesita que la cadena termine
    .ascii "Hola mundo!\n\0"    # en el carácter nulo (\0)

.section .text

# GCC es el compilador de GNU. Puede compilar diversos lenguajes (C, C++, etc.)
# y también ensamblar código ensamblador. GCC también se encarga de enlazar el
# código con las funciones de la biblioteca de C. GCC define la etiqueta
# _start y añade código de iniciación de las bibliotecas de programación y
# toma la función 'main' como el inicio de nuestro programa.
.globl main
main:
    # Cada función compatible con la biblioteca de C debe hacer algunas tareas
    # al iniciarse: salvar el valor de %rbp en la pila, hacer una copia de
    # %rsp, crear espacio para variables locales en la pila, etc. La
    # instrucción "enter" se encarga de todo eso. Los parámetros a cero es
    # porque no necsitamos reservar espacio en la pila.
    enter $0, $0
    # Preparamos la llamada a la función 'printf' de la biblioteca de C
    # Sólo usaremos un parámetro: la dirección del texto a imprimir
    movq $message, %rdi         # parámetro 1: dirección de la cadena de texto
    movq $0, %rax               # %rax=0 si no hay parámetros en p. flotante
    call printf                 # llamada a la función 'printf'

    # Para terminar el programa, en vez de usar la llamada al sistema 'exit',
    # simplemente retornamos de la función 'main'. El código de finalización
    # introducido por GCC se encargará de hacer la llamada a 'exit'.
    movq $0, %rax               # valor de retorno de la función (0)
    leave                       # restaura la configuración creada con 'enter'
    ret                         # llamada a la función 'exit'

/*
    EJERCICIO

    1. Ensambla y enlaza el programa con el siguient comando:

        $ gcc -static -o hello-static hello.s

       GCC ejecutará los comandos necesarios para ensamblar y enlazar nuestro
       programa. Esto incluye enlazar con la biblioteca de C que contiene la
       función 'printf'.

       La opción '-o hello-static' indica el ejecutable a generar.

       La opción '-static' indica que queremos un enlazado estático, esto es, el
       ejecutable generado incluirá tanto nuestro código como el de la función
       'printf' que hemos usado.

    2. Ejecuta el programa y observa si imprime el mensaje esperado:

        $ ./hello-static

    3. Vuelve a ensamblar y enlazar el programa con el siguiente comando:

        $ gcc -no-pie -o hello-dynamic hello.s

       Al eliminar la opción '-static', GCC generará un ejecutable enlazado
       dinámicamente. Esto es, el código de la función 'printf' no se
       incluirá en el ejecutable, sino que será cargado "dinámicamente" cuando
       el usuario ejecute el programa, teniendo así un archivo ejecutable de
       un tamaño mucho menor.

       En la mayoría de las configuraciones actuales, GCC intentará generar
       un programa ejecutable independiente de la posición de la memoria que
       ocupe (PIE) pero escribir ensamblador independiene de la posición es un
       poco más complejo, por lo que la opción '-no-pie' indica a GCC que
       ensamble nuestro programa como un ejecutable tradicional.

    4. Ejecuta la versión dinámica del programa y comprueba que funciona:

        $ ./hello-dynamic

       Compara el tamaño de los archivos ejecutables hello-static y
       hello-dynamic y observa la ventaja de usar enlazado dinámico.

    3. Es posible obtener las bibliotecas dinámicas que usa un programa con el
       comando 'ldd'. Prueba el comando con nuestro programa e intenta
       interpretar el resultado.

        $ ldd ./hello-dynamic

       Prueba el comando 'ldd' con otros archivos ejecutables del sistema. Verás
       el uso extensivo que se hace del enlazado dinámico de bibliotecas de
       programación. Por ejemplo:

        $ ldd /bin/mkdir
        ...

        $ ldd /usr/bin/evince
        ...
 */
