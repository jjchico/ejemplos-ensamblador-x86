# Descripción:  imprime una cuenta de números usando la función 'printf' de
#               la biblioteca de C.
#
# Objetivos:
#   * Mostrar la forma de llamar a funciones de la biblioteca estándar
#     de C desde código ensamblador.
#   * Aprender a llamar a 'printf' desde ensamblador para imprimir
#     mensajes y números.
#   * Uso de 'push' y 'pop' para salvar y recuperar el valor de registros.
#
# Notas:
#   * La lista de parámetros en llamadas a funciones en x86-64 se pasan a la
#     función en los siguientes registros en el orden mostrado:
#       rdi, rsi, rdx, rcx, r8, r9
#   * Cuando los parámetros son números enteros %rax debe valer cero.
#   * El valor retornado por la función se devuelve en el registro %rax si es
#     de 64 bits y en la combinación de registros rdx:rax si es de hasta 128
#     bits.
#   * El puntero de pila %rsp debe estar "alineado" a direcciones múltiplo de 16
#     antes de llamar a cualquier función de la biblioteca de C.
#   * rbx, rbp y r12-r15 son 'callee-saved', so valor no debe cambiar al
#     regresar de la función llamada por lo que printf no cambiará su valor.
#     El resto de registros pueden ser modificados por la función.
#
# Uso de registros:
#       %eax: debe ponerse a cero antes de llamar a printf
#       %rdi: dirección de la cadena de formato
#       %rsi: valor a imprimir

.equ COUNT, 20                  # valor máximo de la cuenta

.section .data
message:                        # mensaje inicial
    .ascii "Contando...\n\0"    # printf necesita que la cadena termine
                                # en el carácter nulo (\0)
format:                         # cadena de formato para imprimir un número
    .ascii "Valor: %lu\n\0"     # %lu indica un número entero de 64 bits sin
                                # signo (long unsigned)
.section .text

.globl main
main:
    enter $0, $0                # preparamos la pila de la función
    # Imprimimos el mensaje inicial
    movq $message, %rdi         # parámetro 1: dirección de la cadena de texto
    movl $0, %eax               # %al=0 si no hay parámetros en p. flotante
    call printf                 # llamada a la función 'printf'

    movq $0, %rsi               # iniciamos el contador %rsi y la cadena de
    movq $format, %rdi          # formato para printf

loop:
    cmp $COUNT, %rsi            # finalizamos si hemos completado la cuenta
    je finish

    movl $0, %eax               # preparamos llamada a printf
    push %rdi                   # salvalos los registros que queremos conservar
    push %rsi                   # en la pila
    call printf                 # llamada a printf
    pop %rsi                    # recuperamos los registros de la pila
    pop %rdi                    # (en orden inverso)

    incq %rsi                   # incrementamos el valor de cuenta y repetimos
    jmp loop

finish:
    # Para terminar el programa, en vez de usar la llamada al sistema 'exit',
    # usaremos la función 'exit' de C que hace la misma tarea.
    leave                       # restauramos la pila original
    movq $0, %rax               # valor de retorno del programa
    ret                         # salimos de 'main' y del programa

/*
    EJERCICIO

    1. Ensambla y enlaza el programa con el siguiente comando:

        $ gcc -no-pie -o count count.s

       El programa generado contendrá el código máquina correspondiente y la
       información necesaria para cargar dinámicamente el código de la
       biblioteca de C cuando el programa sea ejecutado.

       Añade la opción '-g' si quieres que el ejecutable incluya códigos de
       depuración para poder ejecutar el programa con un depurador: gdb, ddd,
       etc.

    2. Ejecuta el programa y observa si imprime el mensaje esperado:

        $ ./count

    3. Prueba a cambiar el número de valores a contar en el programa, ensambla
       y ejecuta de nuevo.

    4. Modifica el uso de registros en el programa de forma que no sea necesario
       ejecutar 'push' y 'pop'. Pista: emplea registros preservados por las
       funcions como r12, r13, etc.
 */
