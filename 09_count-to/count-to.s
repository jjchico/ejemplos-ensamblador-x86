# Descripción:  imprime una cuenta de números usando la función 'printf' de
#               la biblioteca de C hasta un límite introducido por el
#               usuario por la entrada estándar.
#
# Objetivos:
#   * Mostrar la forma de llamar a funciones de la biblioteca estándar
#     de C desde código ensamblador.
#   * Aprender a llamar a 'printf' desde ensamblador para imprimir
#     mensajes y números.
#   * Aprender a llamar a 'scanf' desde ensamblador para leer números
#     desde la entrada estándar.
#
# Notas:
#   * La lista de parámetros en llamadas a funciones en x86-64 se pasan a la
#     función en los siguientes registros en el orden mostrado:
#       rdi, rsi, rdx, rcx, r8, r9
#   * Cuando los parámetros son números enteros %rax debe valer cero.
#   * El valor retornado por la función se devuelve en el registro %rax si es
#     de 64 bits y en la combinación de registros rdx:rax si es de hasta 128
#     bits.
#   * El puntero de pila %rsp debe estar "alineado" a direcciones múltiplo de 16
#     antes de llamar a cualquier función de la biblioteca de C.
#   * rbx, rbp y r12-r15 son 'callee-saved', so valor no debe cambiar al
#     regresar de la función llamada por lo que printf no cambiará su valor.
#     El resto de registros pueden ser modificados por la función.
#
# Uso de registros:
#       %eax: debe ponerse a cero antes de llamar a printf
#       %r12: contador de números

.section .data
message:                        # mensaje inicial
    .ascii "Introduce valores a contar: \0"
scanf_format:
    .ascii "%lu\0"              # cadena de formato para 'scanf': interpreta
                                # el texto introducido como un entero de 64b
                                # sin signo (%lu)
printf_format:                  # cadena de formato para 'printf': imprime
    .ascii "Valor: %lu\n\0"     # un número entero de 64b sin signo (%lu)

.section .bss                   # espacio para almacenar el número introducido
end_val:                        # (parámetro para scanf)
    .skip 8

.section .text

.globl main
main:
    enter $0, $0                # preparamos la pila de la función

    # Imprimimos el mensaje inicial
    movq $message, %rdi         # parámetro 1: dirección de la cadena de texto
    movq $0, %rax               # %al=0 si no hay parámetros en p. flotante
    call printf                 # llamada a la función 'printf'

    # Leemos el número de valores desde el terminal (ver: man 3 scanf)
    movq $0, %rax               # preparamos llamada a scanf
    movq $scanf_format, %rdi    # parámetro 1: cadena de formato
    movq $end_val, %rsi         # parámetro 2: dirección de memoria donde
    call scanf                  # guardar el resultado

    movq $0, %r12               # iniciamos el contador en %r12

loop:
    cmp end_val, %r12           # finalizamos si hemos completado la cuenta
    je finish

    movl $0, %eax               # preparamos llamada a printf
    movq $printf_format, %rdi   # parámetro 1: cadena de formato
    movq %r12, %rsi             # parámetro 2: valor a imprimir
    call printf                 # llamada a printf

    incq %r12                   # incrementamos el valor de cuenta y repetimos
    jmp loop

finish:
    # Para terminar el programa, en vez de usar la llamada al sistema 'exit',
    # usaremos la función 'exit' de C que hace la misma tarea.
    leave                       # restauramos la pila original
    movq $0, %rax               # valor de retorno del programa
    ret                         # salimos de 'main' y del programa

/*
    EJERCICIO

    1. Ensambla y enlaza el programa con el siguiente comando:

        $ gcc -no-pie -o count-to count-to.s

       El programa generado contendrá el código máquina correspondiente y la
       información necesaria para cargar dinámicamente el código de la
       biblioteca de C cuando el programa sea ejecutado.

       Añade la opción '-g' si quieres que el ejecutable incluya códigos de
       depuración para poder ejecutar el programa con un depurador: gdb, ddd,
       etc.

    2. Ejecuta el programa. Introduce un número de valores y observa si imprime
       la salida esperada:

        $ ./count-to
        [...]

    3. Ejecuta el programa varias veces introduciendo diferentes números de
       valores a contar.

    4. Modifica el programa para que haga una cuenta descendente, esto es, si
       se introduce el número 35 el programa deberá contar desde 35 a 0.
 */
