# Descripción: suma dos números en ensamblador. Devuelve el resultado como
#       valor de retorno (propuesto como ejercicio)
#
# Objetivo:
#       * Mostrar un ejemplo muy básico de programación en ensamblador.
#       * Introducir el ensamblado y enlazado de programas en ensamblador.
#       * Introducir las llamadas al sistema operativo (SO).

.section .data          # datos del programa
                        # no hay datos en este programa

.section .text          # código del programa

.globl _start           # '_start' es una etiqueta que indica dónde comienza
_start:                 # el programa. Debe ser un símbolo 'global'

    movq $10, %rax      # rax = 10
    movq $14, %rdx      # rdx = 14
    addq %rax, %rdx     # rdx = rdx + rax

    movq $60, %rax      # 'syscall' activa una interrupción que es el
    movq $0, %rdi       # canal de comunicación con el kernel de SO.
                        # rax indica la función requerida: 60 -> exit
                        # rdi es el valor de retorno del programa
    syscall             # 0 -> todo correcto (ver con 'echo $?')

/*
    EJERCICIO

    1. Ensambla, enlaza y ejecuta el programa:

        $ as -o add.o add.s
        $ ld -o add add.o
        $ ./add

    2. Comprueba el valor de retorno del programa (debe ser 0):

        $ echo $?
        0

    3. Modifica el código para devolver como valor de retorno el resultado
       de la suma. Ensambla, ejecuta el programa y comprueba el valor de
       retorno. Debe ser 24.

    4. Vuelve a ensamblar y enlazar el programa, pero ahora incluye símbolos
       de depuración (opción -g) para poder depurar el código:

        $ as -g -o add.o add.s
        ...

    5. Ejecuta el programa bajo el control del depurador 'ddd' y realiza
       algunas acciones:

        $ ddd add

       En el depurador:

        * Crea un punto de parada al principio del programa: botón derecho
          sobre la primera instrucción, 'Set Breakpoint'.
        * Abre la ventana de registros: Status -> Registers...
        * Inicial el programa: Program -> Run (F2) (o ventana de funciones). El
          programa se detendrá en la primera instrucción.
        * Ejecuta el programa paso a paso: Program -> Step (F5) (o ventana de
          funciones). Observa como cambia el valor de los registros. Comprueba
          que todo funciona como estaba previsto.
 */
