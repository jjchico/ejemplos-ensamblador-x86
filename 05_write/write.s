# Descripción: escribe un mensaje en el terminal usando la llamada
#       al sistema 'write'
#
# Objetivos:
#       * Aprender a mostrar texto en el terminal usando la llamada al
#         sistema 'write'.
#
# Parámetros de la llamada 'write':
#   %rax: número de la llamada al sistema (1 para 'write')
#   %rdi: descriptor del archivo (1 para salida estándar 'stdout')
#   %rsi: puntero al mensaje
#   %rdx: número de bytes a escribir

.section .data
message:
    # '.ascii' guarda los bytes correspondientes a una cadena de texto.
    .ascii "Hola mundo!\n"  # '\n' es el carácter 'fin de línea'
message_end:

# '.equ' permite definir símbolos que representan valores numéricos.
# La definición puede incluir operaciones con otros símbolos.
.equ message_len, message_end - message     # longitud del mensaje

.section .text
.globl _start
_start:
    # preparamos llamada 'write'
    movq $1, %rax               # 1 es el código de 'write'
    movq $1, %rdi               # 1 es el descriptor 'stdout'
    movq $message, %rsi         # puntero al mensaje a escribir
    movq $message_len, %rdx     # longitud del mensaje
    syscall                     # ejecuta la llamada

    # preparamos llamada 'exit'
    movq $60, %rax              # 1 es el código de 'exit'
    movq $0, %rdi               # el valor de retorno es 0 (todo correcto)
    syscall                     # ejecuta la llamada

/*
    EJERCICIO

    1. Ensambla, enlaza y ejecuta el programa. Comprueba que aparece texto
       escrito en el terminal.

    2. Cambia el texto que escribe el programa y vuelve a ejecutarlo.
 */
