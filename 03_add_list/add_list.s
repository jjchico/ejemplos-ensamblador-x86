# Descripción: suma una lista de números. Devuelve el resultado como
#       valor de retorno.
#
# Objetivo:
#       * Introducir los bucles en ensamblador.
#       * Introducir el segmento de datos no inicializados (.bss)

.section .data              # datos del programa
data_list:
    .quad   3, 5, 7, 9, 2, 1, 0

# La sección '.bss' permite reservar espacio para variables y datos que no
# tienen un valor previo asignado, por lo que no ocuparan espacio en el
# archivo del programa. El SO reservará el espacio en memoria al cargar
# el programa
.section .bss
result:
    .skip 8                 # reserva 8 bytes (64bits)

.section .text              # código del programa

.globl _start
_start:
    movq $7, %rcx           # contador de números
    movq $data_list, %rbx   # puntero a la lista de números
    movq $0, %rax           # inicializamos acumulador a cero
loop:
    addq (%rbx), %rax       # sumamos un número de la lista en el acumulador
    addq $8, %rbx           # incrementamos el puntero a la lista
    decq %rcx               # decrementamos el contador de números
    jnz loop                # repetimos si quedan más números

    movq %rax, result       # salvamos el resultado en la memoria

    movq $60, %rax          # finalizamos el programa (llamada a exit)
    movq $0, %rdi
    syscall

/*
    EJERCICIO

    1. Ensambla y enlaza el programa incluyendo códigos de depuración.

    2. Ejecuta el programa bajo el depurador 'ddd'. En el depurador, prepara la
       visualización de la memoria de datos:

       Data -> Memory... Examine: 7, unsigned, giants (8), from: &data_list

       Data -> Memory... Examine: 1, unsigned, giants (8), from: &result

    3. Ejecuta el programa paso a paso y comprueba que funciona correctamente.

    4. Cambia el valor de los datos a sumar y vuelve a ejecutar en el depurador.
       Ten en cuenta que también puedes sumar datos negativos que serán
       representados internamente en notación complemento a 2.

    5. (Avanzado) Modifica el programa para que sume todos los datos de la lista
       hasta que encuentre el primer dato a cero. En este caso ya no hará falta
       usar un registro contador. Ejecuta el programa en el depurador para
       comprobar su correcto funcionamiento.
 */
